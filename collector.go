// Listens to a queue for new jobs to process
package psworker

import (
	"github.com/golang/glog"
	"github.com/streadway/amqp"
)

// A buffered channel that we can send work requests on.
var WorkQueue = make(chan amqp.Delivery, 100)
var ch *amqp.Channel

// Collector connects to a work queue over amqp, reads any work requests
// to process, and then passes them off to a worker. Arguments required are functions
// that control the amqp channel created (stop consuming, cancel, etc.) and a func
// that will increment the number of messages recieved.
func Collector(
	chan_control func(*amqp.Channel, string),
	msg_notify func(int),
	connection_string string,
	exchange_name string,
	exchange_type string,
	queue_name string,
	exchange_durable bool,
	queue_durable bool,
	delete_queue bool,
	auto_ack bool,

) {
	glog.Infof("Connecting to queue at %s\n", connection_string)
	conn, err := amqp.Dial(connection_string)
	if err != nil {
		glog.Error(err)
	}

	defer conn.Close()

	ch, err = conn.Channel()
	if err != nil {
		glog.Error(err)
	}

	defer ch.Close()

	err = ch.ExchangeDeclare(
		exchange_name,    // name
		exchange_type,    // type
		exchange_durable, // durable
		false,            // auto-deleted
		false,            // internal
		false,            // no-wait
		nil,              // arguments
	)

	q, err := ch.QueueDeclare(
		queue_name,    // name
		queue_durable, // durable
		delete_queue,  // delete when unused
		false,         // exclusive
		false,         // no-wait
		nil,           // arguments
	)
	if err != nil {
		glog.Error(err)
	}

	err = ch.QueueBind(
		q.Name,
		exchange_name,
		exchange_name,
		false,
		nil,
	)

	msgs, err := ch.Consume(
		q.Name,   // queue
		"",       // consumer
		auto_ack, // auto-ack
		false,    // exclusive
		false,    // no-local
		false,    // no-wait
		nil,      // args
	)
	if err != nil {
		glog.Error(err)
	}

	forever := make(chan bool)

	// set an externally accessible reference to this consumer for external control
	chan_control(ch, exchange_name)

	go func() {
		for d := range msgs {
			msg_notify(1)
			work := d
			WorkQueue <- work
		}
	}()

	glog.Infof(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

func GetChannel() *amqp.Channel {
	return ch
}
