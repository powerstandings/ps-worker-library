package psworker

import (
	"github.com/golang/glog"
	"github.com/streadway/amqp"
)

var WorkerQueue chan chan amqp.Delivery

// Handles taking jobs from the Collector, and passing them
// to the next available worker
func StartDispatcher(
	nworkers int,
	worker IWorker,
	handler func(map[string]interface{}),
) {
	// First, initialize the channel we are going to but the workers' work channels into.
	WorkerQueue = make(chan chan amqp.Delivery, nworkers)
	glog.Infof("Preparing to start %d workers...\n", nworkers)
	// Now, create all of our workers.
	for i := 0; i < nworkers; i++ {
		glog.Info("Starting worker", i+1)
		plan := worker.NewWorkPlan(i+1, WorkerQueue)
		worker.Start(plan, handler)
	}

	go func() {
		for {
			select {
			case work := <-WorkQueue:
				go func() {
					worker := <-WorkerQueue
					worker <- work
				}()
			}
		}
	}()
}
