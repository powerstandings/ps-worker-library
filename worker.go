package psworker

import (
	"encoding/json"

	"github.com/streadway/amqp"
)

// The Interface your worker must implement.
type IWorker interface {
	Start(
		w WorkPlan,
		handler func(map[string]interface{}),
	)
	Stop(w WorkPlan)
	NewWorkPlan(
		id int,
		workerQueue chan chan amqp.Delivery,
	) WorkPlan
}

// Example worker struct.
type Worker struct {
	IWorker
}

// The work obect structure required by the IWorker interface.
type WorkPlan struct {
	ID          int
	Work        chan amqp.Delivery
	WorkerQueue chan chan amqp.Delivery
	QuitChan    chan bool
}

func GetWorker() *Worker {
	var worker = Worker{}
	return &worker
}

// Example NewWorker function. It should take the ID, and the work channel,
func (worker *Worker) NewWorkPlan(
	id int,
	workerQueue chan chan amqp.Delivery,
) WorkPlan {
	// Create, and return the worker object.
	plan := WorkPlan{
		ID:          id,
		Work:        make(chan amqp.Delivery),
		WorkerQueue: workerQueue,
		QuitChan:    make(chan bool)}

	return plan
}

// The Start() Function. Starts the worker, sends any work to the function you
// pass in as an argument.
func (worker *Worker) Start(
	w WorkPlan,
	handler func(map[string]interface{}),
) {

	var data map[string]interface{}

	go func() {
		for {
			// Add ourselves into the worker queue.
			w.WorkerQueue <- w.Work

			select {
			case work := <-w.Work:
				json.Unmarshal(work.Body, &data)
				handler(data)
			}
		}
	}()
}

// Stop tells the PSN worker to stop listening for work requests.
//
// Note that the worker will only stop *after* it has finished its work.
func (worker *Worker) Stop(w WorkPlan) {
	go func() {
		w.QuitChan <- true
	}()
}
